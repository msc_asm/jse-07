package ru.gasanov.tm;

import ru.gasanov.tm.dao.ProjectDAO;
import ru.gasanov.tm.dao.TaskDAO;

import java.util.Scanner;
import static ru.gasanov.tm.constant.TerminalConst.*;

public class App {

    private static final ProjectDAO projectDAO = new ProjectDAO();

    private static final TaskDAO taskDAO = new TaskDAO();

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(final String[] args) {
        run(args);
        displayWelcome();
        final Scanner scanner = new Scanner(System.in);
        String command  = "";
        while (!CMD_EXIT.equals(command)){
            command = scanner.nextLine();
            run(command);
        }
    }

    private static void run(final String[] args) {
        if (args == null) return;
        if (args.length < 1) return;
        final String param = args[0];
        final int result = run(param);
        System.exit(result);
    }

    private static int run(final String param) {
        if (param == null) return -1;
        switch (param) {
            case CMD_HELP: return displayHelp();
            case CMD_ABOUT: return displayAbout();
            case CMD_VERSION: return displayVersion();
            case CMD_EXIT: return displayExit();

            case PROJECT_CLEAR: return clearProject();
            case PROJECT_CREATE: return createProject();
            case PROJECT_LIST: return listProject();

            case TASK_CLEAR: return clearTask();
            case TASK_CREATE: return createTask();
            case TASK_LIST: return listTask();

            default: return displayError();
        }
    }

    private static int createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("PLEASE ENTER PROJECT NAME");
        final String name = scanner.nextLine();
        projectDAO.create(name);
        System.out.println("[OK]");
        return 0;
    }

    private static int clearProject() {
        System.out.println("[CLEAR PROJECT]");
        projectDAO.clear();
        System.out.println("[OK]");
        return 0;
    }

    private static int listProject() {
        System.out.println("[LIST PROJECT]");
        System.out.println(projectDAO.findAll());
        System.out.println("[OK]");
        return 0;
    }

    private static int createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("PLEASE ENTER TASK NAME");
        final String name = scanner.nextLine();
        taskDAO.create(name);
        System.out.println("[OK]");
        return 0;
    }

    private static int clearTask() {
        System.out.println("[CLEAR TASK]");
        taskDAO.clear();
        System.out.println("[OK]");
        return 0;
    }

    private static int listTask() {
        System.out.println("[LIST TASK]");
        System.out.println(taskDAO.findAll());
        System.out.println("[OK]");
        return 0;
    }

    private static void displayWelcome() {
        System.out.println("* WELCOME TO TASK MANAGER *");
    }

    public static int displayExit() {
        System.out.println(" Terminate programm");
        return 0;
    }

    private static int  displayAbout() {
        System.out.println("Gasanov Asiman");
        System.out.println("gasanov_ad@nlmk.com");
        return 0;
    }

    private static  int displayError() {
        System.out.println("ERROR! You passed wrong parameters ");
        return -1;
    }

    private static int  displayHelp() {
        System.out.println("version - Display program version.");
        System.out.println("about - Display programm info.");
        System.out.println("help - Display list of terminal commands.");
        System.out.println("exit - Terminate console.");
        System.out.println("project-list - Display list of projects.");
        System.out.println("project-clear - Clear list of projects.");
        System.out.println("project-create - Create project.");
        System.out.println("task-list - Display list of tasks.");
        System.out.println("task-clear - Clear list of tasks.");
        System.out.println("task-create - Create tasks.");
        return 0;
    }

    private static int  displayVersion() {
        System.out.println("1.0.0");
        return 0;
    }

}
